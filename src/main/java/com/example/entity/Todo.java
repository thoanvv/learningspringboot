package com.example.entity;

import lombok.Data;

@Data
public class Todo {
    private String title;
    private String detail;
}